FROM debian:wheezy
MAINTAINER pragmatux-users@lists.pragmatux.org

ENV DEBIAN_FRONTEND noninteractive

# Fix the broken ATP sources in image debian:wheezy
ARG keyring_pkg=freexian-archive-keyring_2018.05.29_all.deb
COPY $keyring_pkg /tmp
RUN dpkg -i /tmp/$keyring_pkg
RUN echo "deb http://archive.debian.org/debian wheezy main" >/etc/apt/sources.list \
 && echo "deb [arch=amd64] http://deb.freexian.com/extended-lts wheezy-lts main" >>/etc/apt/sources.list \
 && apt-get update \
 && apt-get -qq upgrade
